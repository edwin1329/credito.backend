import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany
} from "typeorm";
import { Exclude } from 'class-transformer';
import { Credit } from "../../credit/entities/credit.entity";

@Entity({ name: 'users' })
export class User {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    first_name: string;

    @Column()
    last_name: string;

    @Column()
    document: number;

    @Column()
    birth_date: Date;

    @Column()
    @Exclude()
    password: string;

    @CreateDateColumn()
    crated_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @OneToMany(type => Credit, credit => credit.user)
    credits: Credit[];

}