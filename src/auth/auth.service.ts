import { Injectable, BadRequestException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dtos/crate-user.dto';
import { User } from './entities/user.entity';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from './interfaces/jwt-payload';
import { LoginDto } from './dtos/login.dto';

@Injectable()
export class AuthService {

    private saltRounds = 10;

    constructor(
        @InjectRepository(User)
        private readonly userRepository: Repository<User>,
        private readonly jwtService: JwtService
    ) { }

    async find(params: any) {
        return await this.userRepository.findOneOrFail({ document: params.document });
    }

    async create(createUserDto: CreateUserDto): Promise<User> {
        const user = await this.findUserByDocument(createUserDto.document);
        if (user) {
            throw new BadRequestException('Documento ya exite');
        }
        const newUser = this.userRepository.create(createUserDto);
        newUser.password = await this.getHash(createUserDto.password);
        return await this.userRepository.save(newUser);
    }

    async login(loginDto: LoginDto) {
        const user = await this.findUserByDocument(loginDto.document);
        if (user) {
            const passwordSuccess = await this.compareHash(loginDto.password, user.password);
            if (passwordSuccess) {
                const access_token = await this.signJWT({ id: user.id });
                return { access_token };
            }
        }
        throw new BadRequestException('Documento o contraseña incorrectos.');
    }

    async validateUser(payload: JwtPayload): Promise<User> {
        return await this.userRepository.findOne(payload.id);
    }

    async getHash(password: string | undefined): Promise<string> {
        return await bcrypt.hash(password, this.saltRounds);
    }

    async compareHash(password: string | undefined, hash: string | undefined): Promise<boolean> {
        return await bcrypt.compare(password, hash);
    }

    async signJWT(payload: JwtPayload): Promise<string> {
        return await this.jwtService.signAsync(payload);
    }

    private async findUserByDocument(document: number) {
        return await this.userRepository.findOne({ document });
    }
}
