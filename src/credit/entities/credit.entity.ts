import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
    JoinColumn
} from "typeorm";
import { User } from "../../auth/entities/user.entity";

@Entity({ name: 'credits' })
export class Credit {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    nit: string;

    @Column()
    company: string;

    @Column()
    salary: number;

    @Column()
    employed_at: Date;

    @Column()
    approved_credit: number;

    @CreateDateColumn()
    crated_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @ManyToOne(type => User, user => user.credits)
    @JoinColumn({ name: 'user_id' })
    user: User;

}