import {
    ValidatorConstraintInterface,
    ValidationArguments,
    ValidatorConstraint
} from 'class-validator';

@ValidatorConstraint({ name: 'message' })
export class NotEmpty implements ValidatorConstraintInterface {
    validate(input: string) {
        return input && String(input).trim() !== '';
    }

    defaultMessage(validationArs?: ValidationArguments) {
        return `${validationArs.property} es obligatorio`;
    }
}
