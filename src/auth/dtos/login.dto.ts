import { IsNotEmpty } from "class-validator";

export class LoginDto {
    @IsNotEmpty()
    readonly document: number;

    @IsNotEmpty()
    readonly password: string;
}