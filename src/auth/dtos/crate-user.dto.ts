import {
    IsDateString,
    Validate
} from 'class-validator';
import { IsAdult } from '../validations';
import { NotEmpty } from '../../utils/validations';

export class CreateUserDto {
    @Validate(NotEmpty, { message: 'Debes ingresar tu nombre' })
    readonly first_name: string;

    @Validate(NotEmpty, { message: 'Debes ingresar tus apellidos' })
    readonly last_name: string;

    @Validate(NotEmpty, { message: 'Debes ingresar tu documento de identidad' })
    readonly document: number;

    @IsDateString()
    @Validate(IsAdult)
    readonly birth_date: Date;

    @Validate(NotEmpty, { message: 'Debes ingresar una contraseña' })
    readonly password: string;
}
