import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'message' })
export class IsAdult implements ValidatorConstraintInterface {
    validate(input: string) {
        if (input) {
            const date = new Date(input);
            return new Date(date.getFullYear() + 18, date.getMonth(), date.getDate()) <= new Date();
        }
        return false;
    }

    defaultMessage(validationArgs?: ValidationArguments) {
        return `Debes tener más de 18 años para pedir un prestamo`;
    }
}
