import { Injectable } from '@nestjs/common';
import { Credit } from './entities/credit.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateCreditDto } from './dtos/create-credit.dto';
import { User } from '../auth/entities/user.entity';

@Injectable()
export class CreditService {
    constructor(
        @InjectRepository(Credit)
        private readonly creditRepository: Repository<Credit>,
    ) { }

    async create(user: User, creditDto: CreateCreditDto) {
        const credit = this.creditRepository.create(creditDto);
        credit.approved_credit = this.calculateApprovedCredit(creditDto.salary);
        credit.user = user;
        return await this.creditRepository.save(credit);
    }

    private calculateApprovedCredit(salary: number) {
        if (salary >= 800000 && salary <= 1000000) {
            return 5000000;
        } 
        
        if (salary > 1000000 && salary < 4000000) {
            return 20000000;
        }

        return 50000000;
    }
}
