import {
    Controller,
    UseGuards,
    Post,
    Body,
    Req,
    UseInterceptors,
    ClassSerializerInterceptor
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { CreditService } from './credit.service';
import { CreateCreditDto } from './dtos/create-credit.dto';


@Controller('credit')
@UseInterceptors(ClassSerializerInterceptor)
@UseGuards(AuthGuard('jwt'))
export class CreditController {

    constructor(private readonly creditService: CreditService) { }

    @Post()
    async create(@Req() request, @Body() creditDto: CreateCreditDto) {
        return await this.creditService.create(request.user, creditDto);
    }
}
