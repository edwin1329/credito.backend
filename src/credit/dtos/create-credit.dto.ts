import {
    Validate
} from 'class-validator';
import {
    Salary,
    EmployedAt
} from '../validations';
import { NotEmpty } from '../../utils/validations';

export class CreateCreditDto {
    @Validate(NotEmpty, {message: 'Debes ingresar el NIT de la empresa'})
    readonly nit: string;

    @Validate(NotEmpty, { message: 'Debes ingresar la empresa en la que laboras' })
    readonly company: string;

    @Validate(Salary)
    readonly salary: number;

    @Validate(EmployedAt)
    readonly employed_at: Date;
}
