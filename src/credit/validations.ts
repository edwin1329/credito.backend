import {
    ValidatorConstraint,
    ValidatorConstraintInterface,
    ValidationArguments,
} from 'class-validator';

@ValidatorConstraint({ name: 'message' })
export class Salary implements ValidatorConstraintInterface {
    validate(input: number) {
        if (input) {
            return input >= 800000 && input < 100000000;
        }
        return false;
    }

    defaultMessage(validationArs?: ValidationArguments) {
        return `Su salario debe estar entre $800.000 y $100.000.000`;
    }
}

@ValidatorConstraint({ name: 'message' })
export class EmployedAt implements ValidatorConstraintInterface {
    validate(input: string) {
        if (input) {
            const date = new Date(input);
            date.setMonth(date.getMonth() + 18);
            return date <= new Date();
        }
        return false;
    }

    defaultMessage(validationArgs?: ValidationArguments) {
        return `Debe tener mínimo 18 meses de antiguedad en su empresa`;
    }
}
